module.exports = [
	"Крутицких",
	"блоггер",
	"адвего",
	"блоггинг",
	"блоггинга",
	"блоггингом",
	"вертикальнее",
	"верхнеуфалейском",
	"вестные",
	"видеорешение",
	"виджет",
	"виджете",
	"виджеты",
	"вконтакте",
	"времязатратные",
	"вуйчиче",
	"вшэ",
	"гиа",
	"гугл",
	"гугле",
	"забанил",
	"забывку",
	"заинструктировали",
	"законсультировали",
	"захаренко",
	"жаборовского",
	"жд",
	"жердеевой",
	"ютуб",
	"читерский",
	"вебмани",
/*
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
*/
	"переплаканы",
	"переподписаться",
	"переподписываться",
	"почленно",
	"суперсекретами",
	"фгос",
	"инфобизнесмен",
	"инфоурок",
	"матфак",
	"микрофигурки",
	"миниконкурс",
	"мультиурок",
	"неположительна",
	"непристрелянного",
	"неприятненько",
	"непристрелянный",
	"огрн",
	"огэ",
	"однотарифный",
	"опоздайку",
	"подграфика",
	"подграфиком",
	"подлогарифмические",
	"подлогарифмическое",
	"приспособленнее",
	"различнейшим",
	"рособрнадзором",
	"сайтостроению",
	"роботокомплексов",
	"сайтик",
	"сложненькую",
	"самодисциплинируете",
	"спринтхост",
	"сра-сетях",
	"спецпоказатели",
	"скайп",
	"решуегэ",
	"резвенько",
	"техспециальности",
	"техспециальностях",
	"трёхтысячником",
	"угриновича",
	"ужассс",
	"фильмец",
	"фейсбук",
	"фипи",
	"фишечек",
	"фрилансеров",
	"коучинги",
	"директолог",
	"двухтысячником",
// С БВФ (Кольцова)
	"олдовый",
	"юзаю",
	"юзал",
	"юзать",
	"юзает",
	"юзай",
	"юзеров",
	"поюзать",
	"оффтоп",
	"гайцы",
	"кодотел",
	"привате ",// (в привате)
	"пдд",
	"видюха",
	"видюху	",
	"видюхи",
	"стайл",
	"супра",
	"пмм",
	"трабл",
	"траблы",
	"масяня",
	"масяню",
	"схи",
	"вгу",
	"врн",
	"бвф",
	"засранец",
	"баюн",
	"кодотела",
	"глючит",
	"фича",
	"юзера",
	"тестил",
	"адмиралтейка",
	"адмиралтейке",
	"хользунова",
	"куколкина",
	"дрочить",
	"дрочку",
	"чипсете",
	"девайс",
	"перепрошить",
	"стритрейсинг",
	"кафешка",
	"оффтопик",
	"евросети",
	"ворд",
	"няшечка",
	"минет",
	"флейм",
	"плехановской",
	"бсмп",
	"универ",
	"казантип",
	"камелот",
	"системник",
	"сервак",
	"глючить",
	"агузарова",
	"агузарову",
	"челлендж",
	"среднемосковской",
	"евросеть",
	"плехановская",
	"павич",//(Милорад Павич - писатель)
	"чертовицк",
	"машмета",
	"карлсберг", // пиво
	"буби",

// 1cov-edu.ru
	"кардано",
	"риккати",
	"переобозначим",
	"ареакосинус",
	"ареакотангенс",
	"ареасинус",
	"осестремительное",
	"ареакосинуса",
	"ареатангенс",
	"ареакотангенса",
	"ареатангенса",
	"переобозначив",
	"ареасинуса",
	"мфти",
	"яковенко",
	//"соприкасаемых", //возможно, так нельзя
	"взфэи",
	
//webmath
    "твитнуть",
	"рамзаевой",
	"рамзаева",
	"антиплагиат",
	"термех",
	"скайпе",
	"скайпу",
	"подлогарифмической",
	"антиплагиата",

//cleverstudents_фамилии
	"нешков",
	"миндюк",
	"теляковского",
	"атанасян",
	"дудницын",
	"шварцбурд",
	"брадиса",
	"лопиталя",
	"михелович",
	"эльсгольц",
	"жижченко",
	"шабунин",
	"капелли",
	"кардано",
	"бантова",
	"раабе",
	"бельтюкова",
	"брадис",
	"бахурин",
	"вандермонда",
	"соминский",
	"кордемский",

//cleverstudents_термины
	"одз",
	"нод",
	"коллинеарны",
	"параметрически",
	"компланарны",
	"знакоположительный",
	"коллинеарен",
	"антисимметричности",
	"знакоположительного",
	"полуразности",
	"неприведенные",
	"антирефлексивности",
	"домножение",
	"антисимметричность",
	"знакоположительные",
	"неприведенного",
	"невычеркнутым",
	"порязрядное",
	"невыколотой",
	"невыколотая",
	"взаимнообратных",
	"антикоммутативности",
	"знакоположительным",
	"невыколотые",
	"невыколотыми",
	"невыколотых",
	"незачеркнутые",
	"некомпланарны",
	"абелеву",
	"неположительно",
	"домножали",
	"неприведенное",
	"антикоммутативность",
	"неприведенным",
	"неравносильности",
	"неравносильность",
	"домножением",
	"домножения",
	"одиннадцатизначное",
	"знакоположительных",
	"полуразность",
	"полуразностью",
	"полуэллипс",
	"полуэллипса",
	"полуэллипсов",
	"полуэллипсу",
	"антирефлексивность",
//dxdy_термины
	"безызбыточность",
	"безындексное",
	"бессеточные",
	"гамильтонизацией",
	"гамильтонизацию",
	"гиперболически",
	"латех",
	"липшецовости",
	"мультипликативно",
	"по-серьёзному",
	"супермодератор",
	"контрпример",
	"матожидание",
	"логистистических",
	"контрпримеры",
	"учп",
	"липшицевой",
	"чтд",
	"контрпримера",
	"диффуров",
	"диффуры",
	"сильвестера",
	"матан",
	"матана",
	"матожидания",
	"экпоненту",
	"матлаб",
	"сднф",
	"целочисленности",


//dxdy_фамилии
	"лиувиля",
	"сохоцкого",
	"маурера",
	"машерони",
	"менгера",
	"миттаг",
	"миттага",
	"машеров",
	"коровьев",
	"френе",
	"гудермана",
	"кемпнера",
	"коровьева",
	"лиувилля",
	"лобачевкому",

//mathprofi-ru_термины
	"лопиталя",
	"знакопостоянства",
	"фнп",
	"диффурами",
	"нсв",
	"коллинеарны",
	"параметрически",
	"диффур",
	"компланарны",
	"матана",
	"диффуров",
	"прорешать",
	"экселе",
	"диффуры",
	"знакочередование",
	"диффурах",
	"игрековые",
	"прорешали",
	"четырехэтажности",
	"домножаем",
	"решебник",
	"поточечно",
	"диффура",
	"трёхэтажности",
	"эквиваленции",
	"диффурам",
	"эксель",
	"неберущийся",
	"матан",
	"иксовой",
	"неберущимся",
	"коллинеарен",
	"переобозначить",
	"домножением",
	"эквиваленцией",
	"знакочередуется",
	"прорешанных",
	"прорешаны",
	"решебника",
	"прорешанной",
	"разрывна",
	"переобозначим",
	"споловинить",
	"спроецируется",
	"иксовым",
	"игрековую",
	"вышмата",
	"знакочередуются",
	"покоординатно",
	"перестановочно",
	"эквиваленция",
	"домножение",
	"прорешаете",
	"прорешанные",
	"прорешанными",
	"взаимоуничтожаются",
	"игрековый",
	"решебнике",
	"игрековых",
	"неберущиеся",
	"домножений",
	"домножения",
	"иксовый",
	"иксовых",
	"перестановочны",
	"эквиваленцию",
	"антикоммутативности",
	"экселя",
	"иксовые",
	"иксовая",
	"иксовую",
	"игрековая",
	"равновозможность",
	"среднеожидаемое",
	"игрековой",
	"четырёхэтажности",
	"знакоопределенность",
	"знакоопределённость",
	"равновозможности",
	"трехэтажности",
	"взаимоуничтожаться",
	"дифуров",
	"игрековым",
	"левоориентированным",
	"маловозможные",
	"матожиданием",
	"матожиданий",
	"матожиданий",
	"оптимальнее",
	"мучаться",
	"мучался",
	"правоориентированным",
	"правоориентированный",
	"антикоммутативностью",
	"биективная",
	"биективно",
	"биективные",
	"биективным",
	"взаимнообратны",
	"взаимнооднозначность",
	"взаимоуничтожает",
	"высказывательной",
	"высокодисперсионные",
	"диффуру",
	"домножать",
	"домножая",
	"домножении",
	"жордановсих",
	"загроможденности",
	"загромождённости",
	"знакопостоянстве",
	"знакочередования",
	"знакочередоваться",
	"игрекового",
	"иксовое",
	"иксовом",
	"импликационная",
	"импликационной",
	"левоориентированных",
	"маловозможное",
	"матожиданию",
	"матожиданиями",
	"недифференцируемости",
	"неиллюзорную",
	"неколлинеарны",
	"некоммутативности",
	"неположительности",
	"неположительность",
	"неположительны",
	"неравновозможность",
	"неубывание",
	"нештрихованной",
	"ортогонализовать",
	"переобозначу",
	"переообозначим",
	"перестановочна",
	"проинтегрировалось",
	"проинтегрируются",
	"прорешав",
	"прорешал",
	"прорешанная",
	"прорешанного",
	"прорешанном",
	"прорешанную",
	"прорешиваем",
	"прорешивал",
	"прорешивали",
	"прорешивают",
	"прорешивая",
	"равноотрезочного",
	"равносильностями",
	"разнознакового",
	"разнознаковый",
	"сверхподробный",
	"сонаправленность",
	"теорвер",
	"тервер",
	"тервером",
	"трудноприменима",
	"четырёхэтажностью",
	
//mathprofi-ru_фамилии
	"рябушко",
	"раабе",
	"атанасян",
	"атанасяна",
	"чудесенко",
	"гмурмана",
	"гмурман",
	"мартингейл",
	"атанасяну",
	"банкролл",
	"банкроллом",
	"загибулин",
	"лерон",
	"садовничего",

//Ru-math-wikia-com_фамилии
	"мерсенна",
	"банаха",
	"рамануджана",
	"маскерони",
	"фейгенбаума",
	"гельфонда",
	"лемера",
	"мозера",
	"ковальченко",
	"лопиталь",
	"сенько",
	"булеан",
	"лиувилль",
	"гудермана",
	"банах",
	"лагерра",
	"бухштабер",
	"кострикин",
	"загоруйко",
	"зиллион",
	"кантелли",
	"хеллмана",
	"силовской",
	"фубини",
	"вейвлетов",
	"голоборщенко",
	"стьюарт",
	"трибоначчи",
    "финетти",
	"большев",
	"бородкин",
	"булеане",
	"бэггинг",
	"варден",
	"вилански",
	"винберг",
	"виртингера",
	"гливенко",

//Ru-math-wikia-com_термины
	"мультиномиальное",
	"днф",
	"нод",
	"гиперреальные",
	"супердействительные",
	"супернатуральные",
	"мультиномиального",
	"вещественнозначная",
	"пандиагональные",
	"топологически",
	"полупрямое",
	"банах",
	"кососимметричной",
	"контрпример",
	"сднф",
	"бикватернион",
	"октонионы",
	"центиллион",
	"верхнетреугольная",
	"мультипликативность",
	"пандиагональных",
	"паракомпактно",
	"паракомпактом",
	"полусовершенное",
	"репьюнитов",
	"силовских",
	"скоринг",
	"триморфное",
	"векторнозначных",
	"квартилью",
	"комплекснозначных",
	"липшицево",
	"непусто",
	"нумерологический",
	"паракомпакт",
	"паракомпакта",
	"паракомпактность",
	"полусовершенным",
	"сингулярностей",
	"фибоначчиева",
	"антидизъюнкция",
	"антиконъюнкция",
	"антисимметрия",
	"беззнаковые",
	"биективный",
	
//schoolmath_фамилии
	"габибулаев",
	"гушневская",
	"шарыгина",
	
//kpolyakov_spb_ru_Фамилии
	"угринович",
	"лампанель",
	"самылкина",
	
//raum_maht_ru
    "иванищук",
    "астрель",
    "решетняк",
    "денищева",
    "шклярник",	
	"ляховец",
	"кисельников",
	"рамоданов",
	"ведищий",
	
//reshuege
    "неубывания",	
	"лемешко",
	"невозрастания",
	"вирченко",
	"камзеевой",
	"сиротенко",
	"волченко",
	"светопоглощаемость",
	"светопреобразователи",
	"сегнеровым",
	
//alexlarin	
	"корянов",
	"видеоразбор",
	"демовариант",
	"демоварианта",
	"видеорешения",
	"видеолекции",
	"демовариантом",
	
	
//ege_trener	
	"биссекторной",
	"ларинские",
	"ларинских",
	"видеолекция",
	"видеолекции",
	"видеозанятий",
	"хабенского",
	"ларинский",
	"непродвинутого",
	"сложносоставленное",
	"вассимирский",
	"забабурин",
	"стобалльника",
	"алгоритмичный",
	"видеолекцию",
	"видеолекциях",
	"видеомодели",
	"видеоразбора",
	"видеоразборов",
	"видеоразборы",
	"мгту",
	"невыпиленный",
	"недопрыгали",
	"ортотреугольника",
	"подслучаи",
	"предпредпоследнего",
	"структурированно",
	
//fipi_ru	
	"рособрнадзора",
	"рособрнадзор",
	"видеоконсультации",
	"цыбулько",
	"станченко",
	"добротина",
	"дудова",
	"лисковой",
	"артасова",
	"рохлова",
	"шаулин",
	"поздноослепших",
	"бархатова",
	"басюк",
	"бехтиной",
	"видеопособия",
	"внутришкольного",
	"инфокомпас",
	"надпредметный",
	"ромбальский",
	"специалитет",
	"шустанова",
	"раисовна",
	"эверсон",
	"аксакалова",
	"акселс",
	"арваниди",
	"асмолов",
	"аспрял",
	"атажанович",
	"аудиогарнитуры",
	"багаутдинова",
	"багге",
	"байханов",
	"байхановым",
	"баласс",
	"бахтиёрович",
	"беннерс",
	"бик",
	"богатин",
	"брайна",
	"броер",
	"брукингсе",
	"вайтехович",
	"валихов",
	"вальбонне",
	"вальдман",
	"вательский",
	"вентана",
	"вершур",
	"веттона",
	"видеоконсультация",
	"видеолекций",
	"гевуркова",
	"гельмутович",
	"гививна",
	"гиголо",
	"голодец",
	"голубковские",
	"горемыко",
	"городняя",
	"грунюшкина",
	"дейкало",
	"денищевой",
	"держицкая",
	"джеффом",
	"дистракторов",
	"диёрбек",
	"доган",
	"духаниной",
	"закировна",
	"златопольский",
	"зохидович",
	"зупарходжаева",
	"инфографике",
	"кабаева",
	"кабаевой",
	"капустняк",
	"карачевцева",
	"кезина",
	"кирабаев",
	"клопотова",
	"ковальков",
	"компетентностного",
	"компетентностном",
	"кучкаров",
	"кшенникова",
	"лидухен",
	"линдстром",
	"макарчук",
	"марасанов",
	"марпут",
	"марцмана",
	"мгопу",
	"мгпу",
	"микаэльяна",
	"миксюк",
	"мирвалиев",
	"мирэа",
	"мишакова",
	"музаффар",
	"надпредметного",
	"никулинская",
	"ненарушение",
	"норденбо",
	"ноткиной",
	"нурминский",
	"орхусского",
	"паблишерз",
	"павлюшина",
	"падилла",
	"петтер",
	"понтонье",
	"практикоориентированная",
	"раджабов",
	"радиоэфир",
	"райффайзенбанк",
	"реморенко",
	"росаккредагентства",
	"рособрнадзоре",
	"рыжко",
	"саливон",
	"сардор",
	"севара",
	"сергоманов",
	"серикович",
	"сикали",
	"скархейм",
	"скфо",
	"собирович",
	"соломиным",
	"тгу",
	"теровой",
	"тестологии",
	"тестологическую",
	"тригон",
	"трубанева",
	"уайтхерстом",
	"удельнинская",
	"улугбекович",
	"файзрахманова",
	"хасиева",
	"хильде",
	"хисар",
	"хлытина",
	"хотько",
	"шахабас",
	"шемелюк",
	"шляйхер",
	"шокиров",
	"шуклина",
	"щелкина",
	"щербаковская",
	"щёлкина",
	"эргашевич",
	"якобчук",
	"головинское",
	"литературоцентрично",
	"литературоцентричного",
	"литературоцентричность",
	
//egemaximum	
	"егэтренер",
	"видеолекцию",
	"брахмагупты",
	"вампириал",
	"испаньола",
	"шаблончик",
	"шедевральное",
	"видеодоказательств",


//nuru-ru_термины
	"ву",
	"компланарны",
	"коллинеарны",
	"некоррелированы",
	"ненаступления",
	"нестационарен",
	"одномодальное",
	"плосковершинности",
	"разрывна",
	"двухмодальным",


//ru_solverbook_com_термины
	"одз",
	"неберущиеся",
	"полупроизведение",
	"верхнетреугольной",
	"подлогарифмическая",
	"подлогарифмическую",
	"подмодульных",
	"полупроизведению",



//ru_solverbook_com_фамилии
	"ролля",
	"слау",
	"саррюса",
	"лопиталем",
	"лопиталю",
	
//ege-ok_термины_и_фамилии
    "подмодульное",
	"минкевич",
	"высокоранговые",
	"подмодульные",
	"подмодульные",
	"минкевича",
	"минкевичем",
	"подмодульного",
	"высокоранговым",
	"высокоранговых",
	"низкоранговые",
	"ремаренко",
	"незачеркнутыми",
	"симутин",
	"симутина",
	"симутиным",
	"икеда",
	"миларепа",
	"дайсаку",
	"хуэйнэн",
	"шипиловская",
	"файлохранилище",
	"златковича",
	"левочский",
	"неалгебраически",
	"полуабстракция",
	"пшеничникова",
	"силлабус",
	"расcтояние",//вторая "с" латинская

//algebraclass_ru_термины
	"знакопостоянства",
	"неперестановочно",

//matematikalegko_слова
	"авс",
	"аов",
	"чертополошек",
	"электробук",
	"псевдонейрона",
	"единоразовую",
	"равновозможность",
	"равнонаклонены",
	"тангесоиды",
	
//matematikalegko_фамилии
	"бурмистренко",
	"шевела",
	"калдаева",
	"федотовская",
	"лобаченков",
	"посицельская",
	"посицельский",
	"пратусевич",
	"цукенберг",
	"ягубова",
	"смитсона",
	
//forum_exponenta_ru_термины_и_слова
	"логонетик",
	"фев",
	"июн",
	"июл",
	"логотехнологии",
	"критикоустойчивости",
	"маткаде",
	"маткад",
	"психоконструирование",
	"плиз",
	"логотехнология",
	"мапле",
	"психонетики",
	"гиперконцентрации",
	"критикоустойчивым",
	"психонетиков",
	"матлаб",
	"мэпла",
	"параметрически",
	"мэпл",
	"психосистемы",
	"мнеморегрессии",
	"гиперконцентрация",
	"психоконструирования",
	"маткада",
	"мэпле",
	"матану",
	"матлабе",
	"мыслетехника",
	"психоконструктора",
	"свойствология",
	"логотехнологическая",
	"терверу",
	"михлина",
	"маткадом",
	"ленгфорда",
	"критикоустойчивой",
	"псевдологические",
	"псевдологичность",
	"псевдологичные",
	"лжелогичные",
	"лжелогичность",
	"матпакете",
	"психоконструкторы",



//hijos_ru_фамилии
	"мэвис",
	"сейкин",
	"лопиталя",
	"уайлс",
	"жулиа",
	"мерсенна",
	"строун",
	"аньези",
	"шерил",
	"сегерман",
	"маккеллар",
	"тебо",
	"вивиани",
	"тао",
	"рамануджан",
	"сегермана",
	"капрекара",
	"татт",
	"мэйнарда",
	"макларти",
	"сингх",
	"яриам",
	"дилли",
	"чевы",
	"энигма",
	"мангареву",
	"мирзахани",
	"блетчли",
	"уайлса",
	"вольфскеля",
	"филдса",
	"евдокс",
	"евдокса",
	"клэя",
	"фернандес",
	"мориарти",
	"найтингейл",
	"триз",
	"энсисо",
	"таппера",
	"коллатца",
	"скьюза",
	"померанс",
	"лерер",
	"вольфскель",
	"аргана",
	"рамануджана",
	"штейнгарц",
	"понарин",
	"пачоли",
	"лопиталь",
	"вудина",
	"сингха",
	"лишрел",
	"банаха",
	"бреннон",
	"пэррот",
	"гориэли",
	"мачин",
	"гиппопеда",
	"метон",
	"войнича",
	"трикс",
	"вудин",
	"гранди",
	"сейкина",
	"джефф",
	"шеба",
	"косматеско",
	"монтемурро",
	"мойс",
	"флоренс",
	"бретшнайдера",
	"чева",
	"смэйл",
	"харим",
	"татта",
	"хамед",
	"теда",
	"банни",
	"реньи",
	"нейтан",
	"боушер",
	"сидис",
	"бхаскара",
	"сриниваса",
	"штакельберга",
	"брахмагупта",
	"тамбе",
	"крофт",
	"цорна",
	"каснер",
	"ласло",
	"дероуз",
	"могильнер",
	"зейфус",
	"зейфуса",
	"брисон",
	"нокс",
	"нокса",
	"клауди",
	"руперт",
	"барнер",
	"ванцель",
	

//hijos_ru_термины
	"нод",
	"квадрирования",
	"квадрировать",
	"версинус",
	"квадрирован",
	"гаверсинуса",
	"смежны",
	"веркосинус",
	"гаверсинус",
	"гуголплекс",
	"самоподобия",
	"контрпример",
	"подмодульных",

//hijos_ru_комментарии_фамилии
	"шерил",
	"дененберг",
	"пупкин",
	"бутто",
	"лопиталя",
	"штейнгарц",
	"гольхового",
	"ветчинникова",
	"ванцеля",
	"миллена",
	"сканави",

//calcs_su
	"автостраховщиков",
	"европротоколу",
	"квадрициклы",
	"миффлина",
	"морбидное",
	"обоюдка",
	"предожирение",

//cleverstudents
	"сверхспособностями",

//axelarin_net
	"надежкина",
	"видеоразборы",
	"мгту",
	"латанова",
	"нияу",
	"гвэ",
	"понселе",
	"джинчвелашвили",
	"осиповского",
	"корогодова",
	"превью",
	"вышнеградский",
	"корянова",
	"осиповский",
	"банаху",
	"видеоконсультация",
	"видеорешениями",
	"демовариантов",
	"демоварианты",
	"дудрович",
	"кайева",
	"лайфхаки",
	"лайфхак",
	"малаховский",
	"мильков",
	"новокубанский",
	"пляжник",
	"цепелёва",
	"шеховцова",
	"автокоррелированности",
	"автокоррелированными",
	"алейникова",
	"бари",
	"бартельс",
	"батеньковым",
	"бойяи",
	"больцани",
	"боссюе",
	"брадатый",
	"брашман",
	"бютнер",
	"вандивер",
	"варгина",
	"вейгеля",
	"взаимнодополняемая",
	"видеолекций",
	"вышнеградского",
	"гетероскедастичными",
	"гиерон",
	"гиерона",
	"горяиновым",
	"горяиновых",
	"заичкин",
	"звавич",
	"кальмиус",
	"кастри",
	"клиши",
	"кованцов",
	"коляновского",
	"крысицкий",
	"арбалетова",
	"малоинформированные",
	"монферрана",
	"нассауского",
	"недостаев",
	"озаровская",
	"профтестирование",
	"равнозамедленно",
	"разрывны",
	"росэнергоатома",
	"ротмистрова",
	"саммат",
	"самокорректируются",
	"сиракузский",
	"славутский",
	"специалитета",
	"старова",
	"трехшаговый",
	"уайлз",
	"уайлза",
	"шаулина",
	"шеретов",
	"шестнадцатиугольника",
	"эйгес",
	"файлообменники",
	"фгуипп",
	"ферматисты",
	"фмк",
	
];
